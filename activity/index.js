const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4001;

mongoose.connect(`mongodb://lrndlacrz:admin123@ac-mvlgpdf-shard-00-00.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-01.c2nhgts.mongodb.net:27017,ac-mvlgpdf-shard-00-02.c2nhgts.mongodb.net:27017/S35?ssl=true&replicaSet=atlas-ika0ge-shard-0&authSource=admin&retryWrites=true&w=majority`, {
		useNewUrlParser: true,
		useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, 'Connection'))
db.once('open',() => console.log('Conneted to MongoDB'))

const newSchema = new mongoose.Schema({
	username: String,
	password: String
});

const Signup = mongoose.model('Signup', newSchema)

app.use(express.json());
app.use(express.urlencoded({extended: true}))

app.post('/signup', (request, response) => {
	Signup.find({username: request.body.username}, (error,result) => {
		if(error){
			return response.send(error)
		} else if (result != null || result.username == request.body.username) {
			return response.send("Username is already exist")
		} else {
			let newUser = new Signup({
				username: request.body.username,
				password: request.body.password
			})
			newUser.save((error, savedUser) => {
				if(error){
					return response.send(error)
				} else {
					return response.status(201).send('New user registered!')
				}
			})
		}
	})
})


app.get('/signup', (request, response) => {
	Signup.find({}, (error, results) => {
		if(error){
			return response.send(error)
		} else {
			return response.status(200).json({
				signup: results
			})
		}
	})
})

app.listen(port, () => console.log(`Server is running at port: ${port}`))